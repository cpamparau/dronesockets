package com.example.mambodrone;

import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class TcpServer implements Runnable{
    private static final String TAG = "TcpServerThread";

    private boolean running = true;

    private static TcpServer instanta = null;

    public boolean ToLeft = false;

    public boolean ToRight = false;

    public static TcpServer getInstanta(){
        if (instanta == null) {
            instanta = new TcpServer();
        }

        return instanta;
    }

    private TcpServer() {
        super();
    }

    @Override
    public void run() {
        final int port = 5555;
        final String ip = "192.168.10.232";

        try (final ServerSocket serverSocket = new ServerSocket(port)) {
            Log.d(TAG, "Server started, accepting connections to " + ip + ":" + port);

            while (running && !Thread.currentThread().isInterrupted()) {
                final long started = System.currentTimeMillis();
                try (final Socket clientSocket = serverSocket.accept()) {
                    clientSocket.setKeepAlive(true);

                    final long established = (System.currentTimeMillis() - started);
                    Log.d(TAG, "[Connection made at]:" + clientSocket.getRemoteSocketAddress() + " in mills :" + established);

                    this.handleData(clientSocket);

                } catch (final IOException e) {
                    Log.d(TAG, "Failure handling inbound connection to " + ip + ":" + port, e);
                    continue;
                }
            }
        } catch (final IOException e) {
            Log.d(TAG, "Unable to open socket " + ip + ":" + port, e);
            return;
        }
    }

    public void abort() {
        this.running = false;
    }

    private void handleData(final Socket clientSocket) throws IOException {
        try (final DataInputStream stream = new DataInputStream(clientSocket.getInputStream())) {

            final byte[] inBytes = new byte[200];
            int input = 0;
            while ((input = stream.read(inBytes)) != -1) {
                Log.d(TAG, "Size read in bytes: " + input);
                String s = new String(inBytes, StandardCharsets.UTF_8);
                Log.d(TAG, "Data is: " + s);
                if (s.contains("dreapta")){
                    ToRight = true;
                }else if (s.contains("stanga")){
                    ToLeft = true;
                }
            }
        }
        catch (IOException e){
            Log.d(TAG, "Exception: " + e.getMessage());
        }
    }
}
