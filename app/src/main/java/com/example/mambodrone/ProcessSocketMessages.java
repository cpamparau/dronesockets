package com.example.mambodrone;

import android.util.Log;

public class ProcessSocketMessages implements Runnable{
    private static final String TAG = "ProcessSocketMessages";
    private MiniDrone miniDrone = null;

    public ProcessSocketMessages(MiniDrone ss){
        miniDrone = ss;
    }
    @Override
    public void run() {
        if (TcpServer.getInstanta().ToLeft){
            TcpServer.getInstanta().ToRight = false;
            if (miniDrone != null){
                //miniDrone.setYaw((byte)-50);
                Log.d(TAG, "[ToLeft]Drona OK!");
            }
            else{
                Log.d(TAG, "[ToLeft]Drona NULL!");
            }
        }
        else if (TcpServer.getInstanta().ToRight) {
            TcpServer.getInstanta().ToLeft = false;
            if (miniDrone != null){
                //miniDrone.setYaw((byte)50);
                Log.d(TAG, "[ToRight]Drona OK!");
            }else{
                Log.d(TAG, "[ToRight]Drona NULL!");
            }
        }
    }
}
